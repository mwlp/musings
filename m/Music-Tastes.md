@def title = "An Overview of My Music Tastes"
@def date = Date(2021, 04, 27)

![](/media/lastfm-genres.png)

My [last.fm profile](https://www.last.fm/user/mwlp) attempts to track all the songs I listen to through Spotify and YouTube, getting most of it right. As I tend to shuffle my Spotify Liked Songs playlist (or replay my latest earworms over and over), the tracks here are unlikely to be my favorite songs but rather songs that remained at the "recently added" position in my likes during one of my frequent discovery dry-spells.)

If I had to hand-write out my tastes, it would be fairly similar to the above. Heavily electronic - specifically electro and techno. Also lots of Jazz, mainly from a mixture of Jazz playlists on Spotify to random channels that YouTube recommends me.

I have a [YouTube playlist](https://www.youtube.com/playlist?list=PL7UcUP4d7iaEE7ZK8_AWvFVY0TYvVvfeG) that I try to save cool music to. Much of it originates from my freshman year of college, when I was listening to music for most of my waking hours - a lot of it from the game [Osu!](https://osu.ppy.sh/home) I have a [second playlist](https://www.youtube.com/playlist?list=PL7UcUP4d7iaH_Do-WIae5rc4iG2-JpsvN) that I occasionally update with more obscure electronic finds - most of them from /mu/'s /bleep/ threads.)

*Before I list some of the music I like, here's a roadmap for your convenience:*
\toc

## Artists by Genre

### Jazz

- UNT One O'Clock Lab Band
    - ​Dizzy Gillespie - Things to Come (1946)​
    - ​Yesterdays​
- Ruby Rushton
    - ​IRONSIDE​

### Electro
- Boys Noize
- Justice

### Techno
- Mr Oizo
    - Lamb's Anger
    - The Church
- Aphex Twin
    - Syro

### Breakcore
- Venetian Snares
    - Rossz Csillag Alatt Született
        - Masodik Galamb
    - Frictional Nevada
    - Aaron 2
    - Kyokushin

### House
- Daft Punk

### Dubstep
- Skream
    - ​Skream b2b Disclosure Boiler Room DJ Set at W Hotel London​
- Burial
    - Untrue - Archangel

## An Outdated List but a List Nonetheless
During fall 2019 midterms, I typed up a list of some of my favorite songs for a friend that asked for recommendations. Here is that list:

- [Jazz / Lounge Adam Nitti - The Renaissance Man/Rebirth](https://www.youtube.com/watch?v=M0QfkE811SE)
- [Antonio Sánchez - Leviathan](https://open.spotify.com/track/26AIF8kPuT96dIhLjkQFSa) (couldn’t find a YT link)
- [Ari Hoenig - Arrows and Loops](https://www.youtube.com/watch?v=aYaJmFDk_SM)
- [Manhattan Jazz Orchestra - Toccata and Fugue](https://www.youtube.com/watch?v=e0tzCc1NhaY)
- [Eiji Nakayama - Aya's Samba](https://www.youtube.com/watch?v=O7WZYEO9njM)
- [Katamari Damacy OST - Katamaritaino (Roll Me In)](https://youtu.be/1Qfm2WRY8Us)
- [Electronic Masaki Takada - KORG volca beats/bass/keys : dub techno](https://youtu.be/qvCechdVIYY)
- [Mr. Oizo - The Church](https://www.youtube.com/watch?v=pP1kOPFOt-k)
- [Rebeat feat. Jacques Brel - Ne Me Quitte Pas](https://youtu.be/y9LuKHunXYI)
- [Soulwax - Close to Paradise](https://www.youtube.com/watch?v=P41203oiz8Q)
- [Chapter One (Klovn Mix)](https://www.youtube.com/watch?v=lZW6xOOjnVE)
- [ISAN - Lent Et Douloureux](https://www.youtube.com/watch?v=ysDNDoHU3T8)
- [BOYS NOIZE - Ich R U (Justice Remix)](https://www.youtube.com/watch?v=Ah3TrLr351o)
- [The Chemical Brothers - We’ve Got To Try](https://www.youtube.com/watch?v=S07hj7XpD-4)
- [Junkie XL - Twilight Trippin](https://www.youtube.com/watch?v=vZbtR-7rxY4)
- [Masaki Takada - TR-606/TB-303/SPACE/ES-2(The Day of 606)](https://youtu.be/Rt04Yu7MJk8)
- [Aries - Make Me](https://youtu.be/G6YRwi_LAe4)
- [Cardopusher - Crystal Nightcap](https://www.youtube.com/watch?v=8_-tmTxoV5g)
- [Cirez D - Glow](https://www.youtube.com/watch?v=vpPruqqfWQk)
- [Cirez D - On Top Baby](https://www.youtube.com/watch?v=H0458NE13Zk)
- [Modeselektor, Thom Yorke - Shipwreck](https://www.youtube.com/watch?v=qc8iu08o87w)
- [Mafatal – Love Song (Mule Driver Edit)](https://www.youtube.com/watch?v=9J5jTqWTcMo)
- [Aphex Twin - syro u473t8+e [141.98] (piezoluminescence mix)](https://youtu.be/IND_Do8g2w0)
- [CORE - Simpukka chilli](https://youtu.be/mHdVU-Q5RF8) (great with a subwoofer)
- [These albums Let the Children Techno](https://open.spotify.com/album/7Dhx3vMHY23gSUFfiSCFqQ)
- [Mr. Oizo - Lambs Anger](https://open.spotify.com/album/5trGnTKKV1Iz39gXQiAFwZ)
- [found on mu’s /bleep/ Source - Release It (1993)](https://youtu.be/h4P73t-6FKo)
- [The Wise Caucasian - Kutchie Dub (Sushitech Records)](https://youtu.be/9QYtiNOEnRM)
- [Grema - Delays (PAEONI001)](https://youtu.be/0416937R_ck)
- [DnB Mafia - 1995 Flavour [World Bass Records]](https://youtu.be/5dEp_vlz7aw)
- [Weeb Shit 【東方ボーカル】 「月影少女」 【Syrufit】](https://youtu.be/4erQLutWehM)
- [【東方ボーカル】 「Fall In The Dark」 【ShibayanRecords】【Subbed】](https://youtu.be/JwWMpspzcg8)
- [Rock FUUDGE - Satan](https://fuudge.bandcamp.com/track/satan)
- [Weeb Shit 【劇場版ガールズ＆パンツァー】Säkkijärven Polkka【Hard Rock Edition】](https://youtu.be/xAxMdFmmzQk)
- [Other gomen’nasai - 女の子](https://gomennasai.bandcamp.com/track/--2)
- [[Touhou Vocal] [Buta-Otome] Utakata (spanish & english subtitles)](https://youtu.be/t4WCGzynvoo)
- [Shira Choir Sings New Song At Bar Mitzvah](https://youtu.be/ckVYO9oI8vc) (jew & red army songs pair well with Titanfall 2)
- [Religious](https://radio.kiku.fm/aru)