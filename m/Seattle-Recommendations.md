@def title = "Things to do in Seattle"
@def subtitle = "A few recommendations."
@def tags = ["seattle", "travel", "reccs"]
@def date = Date(2022,07,04)

## Pike's Place
The main touristy thing to do when visiting Seattle. Plenty of small shops and markets with fruit, seafood, coffee.
The original Starbucks is here. The coffee certainly isn't worth the line, but if you have a family member that likes Starbucks, they sell special mugs here. If you want Starbucks coffee, I recommend visiting the roastery in Capitol Hill. Otherwise there's plenty of other great coffee around the city.
It isn't entirely obvious to the first time visitor, but there are several layers of shops below the street level. The entrances aren't too hard to find.

## Bainbridge Island
The ferry to Bainbridge is right next to Pike's Place. Most of the experience of this island is the shops and knickknacks on main street. On Saturdays they have a farmer's market. 

## Seattle Art Museum
Free entry (or donation). Amazing collection. Was a meditative experience visiting early in the morning on a weekday when it was mostly empty. Not sure if it'll be empty in the summer, but maybe.

## Discovery Park
Paid entry. A few nice hiking trails. A cool beach with a lighthouse.

## Kerry Park
Cool skyline view of Seattle. Recommend to visit at night for the city lights.

## Space Needle
I believe tickets are ~\$40 per person. It's a decently neat experience, can be skipped if you'd rather spend money elsewhere. If you do go, I'd recommend going around afternoon so you can watch the sunset.

## Chihuly Garden and Glass
Next to the space needle, neat collection of glass sculptures.

## Seattle Underground Tour
I haven't done this myself yet, but it's another common tourist activity that is well spoken of. It is a guided tour of the subterranean ruins of Seattle buried after an 1889 fire.

## Botanical gardens:
The Bellevue one is pretty good. Haven't been to the UW one, but it might be better.

## University of Washington
Very pretty campus. Can be enjoyable to walk around and explore it. Things may have changed, but I normally try to park in their library parking lot and pretend I go there.

## International District
Good asian food, especially Japanese.

## Ballard Wharf
Haven't been here personally, but there's plenty of good seafood and shops around.

## Alki Beach
In West Seattle (which should really be named west of seattle), which can be a bit of a drive, but is nice to walk along and has good sights.

## Biking
- **Redmond** has an amazing collection of bike lanes along their Sammamish River Trail. The entire city is great to explore with a bike, dubbed “The Bicycling Capitol of the Northwest”. I suppose you could rent a bike from one of the several shops on every street corner.
- **Eastlake** (area of Seattle) also has some nice biking trails. I don't know much about them though, so you'd have to look it up.

## Museums (other than ones previously mentioned)
- **Boeing Museum of Flight**: If you like planes or rockets, they have a lot of them here. It's sponsored by Boeing so they also have an exhibit that goes over the history of Boeing.
- **Pop Culture Museum**: If you like a lot of music from the last century, I'm sure they'll have an exhibit on a few of your favorite artists.
- **The Center for Wooden Boats**: Just a bunch of restored wooden boats and a small workshop/museum for the people who restore them. Nice to look at them and the water. Next to the innovation museum (which I do not recommend).

## Far-ish from Seattle
- **Snoqualmie Falls**: Cool waterfall about 25 minutes south of my house
- **Rattlesnake Ledge**: Cool views, nice hike.
- **San Juan Islands / Orcas Islands**: It's about a 2 hour drive to the Anacortes Ferry (another lengthy ride) up to these islands near the border of Canada. Some nice whale watching up here, along with shops similar to those found on Bainbridge.
- **Yakima River**: Fun fly fishing. Booking a guide for half a day is ~\$650.