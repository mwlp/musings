function hfun_bar(vname)
  val = Meta.parse(vname[1])
  return round(sqrt(val), digits=2)
end

function hfun_m1fill(vname)
  var = vname[1]
  return pagevar("index", var)
end

function hfun_musedate()
  date = locvar("date")
  return Dates.format(date, "yyyy/mm/dd")
end

function lx_baz(com, _)
  # keep this first line
  brace_content = Franklin.content(com.braces[1]) # input string
  # do whatever you want here
  return uppercase(brace_content)
end

# source: https://github.com/rikhuijzer/huijzer.xyz/ ,
# who sourced it from: https://github.com/abhishalya/abhishalya.github.io
function hfun_musings()
  today = Dates.today()
  curyear = year(today)
  curmonth = month(today)
  curday = day(today)

  list = readdir("m")
  filter!(endswith(".md"), list)
  function sorter(p)
      ps  = splitext(p)[1]
      url = "/posts/$ps/"
      surl = strip(url, '/')
      pubdate = pagevar(surl, :published)
      if isnothing(pubdate)
          return Date(Dates.unix2datetime(stat(surl * ".md").ctime))
      end
      return Date(pubdate, dateformat"yyyy-mm-dd")
  end
  sort!(list, by=sorter, rev=true)

  io = IOBuffer()
  write(io, """<ul class="blog-posts">""")
  for (i, post) in enumerate(list)
      if post == "index.md"
          continue
      end
      ps  = splitext(post)[1]
      url = "/m/$ps/"
      surl = strip(url, '/')
      title = pagevar(surl, :title)
      subtitle = pagevar(surl, :subtitle)
      pubdate = pagevar(surl, :published)
      description = pagevar(surl, :rss_description)
      if isnothing(pubdate)
          date = "$curyear-$curmonth-$curday"
      else
          date = Date(pubdate, dateformat"yyyy-mm-dd")
      end
      write(io, """<li class="post"><span>""")
      write(io, """</span><b><a href="$url">$title</a></b>""")
      if !isnothing(subtitle)
        write(io, """<li><i class="description">$subtitle</i></li>""")
      end
      
  end
  write(io, "</ul>")
  return String(take!(io))
end

